import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class appTest {

    @Test
    void testableFunction() {
        App app = new App();
        String input = "test";
        assertEquals(app.testableFunction(input), "output = " + input);
    }

    @Test
    void testFunction() {
        App app = new App();
        String input = "test";
        assertEquals(app.testFunction(input), "test = " + input);
    }
}